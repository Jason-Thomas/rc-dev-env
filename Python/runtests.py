import unittest, xmlrunner
from data import *
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_bubbleSort(self):
        self.assertEqual([2, 6, 6, 29, 43, 46, 69, 73, 92, 95], bubbleSort(list1, 1))
        self.assertEqual([95, 92, 73, 69, 46, 43, 29, 6, 6, 2], bubbleSort(list1, -1))
        self.assertEqual([-40, -30, -20, -10, 0, 10, 20, 30, 40, 50], bubbleSort(list2, 1))
        self.assertEqual([50, 40, 30, 20, 10, 0, -10, -20, -30, -40], bubbleSort(list2, -1))


if __name__ == '__main__':
   with open('unittest.xml', 'w') as output:
       unittest.main(
            testRunner=xmlrunner.XMLTestRunner(output=output),
            failfast=False,
            buffer=False,
            catchbreak=False
		)






