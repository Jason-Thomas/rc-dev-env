''' 

Sorting data is one of the most important computing applications. Python lists provide a sort method. In this question, you will implement 
your own sorting function: bubbleSort, using the bubble sort approach. In the bubble sort, the smaller values gradually “bubble” their way 
upward to the top of the list like air bubbles rising in water, while the larger values sink to the bottom of the list. The process that 
compares each adjacent pair of elements in a list in turn and swaps the elements if the second element is less than the first element is 
called a pass. The technique makes several passes through the list. On each pass, successive pairs of elements are compared. If a pair is 
in increasing order, bubble sort leaves the values as they are. If a pair is in decreasing order, their values are swapped in the list.
After the first pass, the largest value is guaranteed to sink to the highest index of a list. After the second pass, the second largest 
value is guaranteed to sink to the second highest index of a list, and so on.
Notes

•	The method bubbleSort takes two input arguments: a list of integers and an integer (named order) to indicate the whether the sorting 
should be ascending or descending. If the order value is 1 means ascending sorting and if it is -1 that means descending order.
bubbleSort(bList, order)

***REVIEWER COMMENTS/APPROVAL****





'''

def bubbleSort(bList, order):  #order: 1 means ascending, -1 means descending
    # make len( bList ) passes
    for item in range( len( bList ) - 1 ):
        # make one pass through len( bList ) - 1 elements
        for index in range( len( bList ) - 1 ):
            # if this element is greater than next element,
            # swap elements.
            if order == 1 and (bList[index] > bList[index + 1]):
                bList[index], bList[index + 1] = bList[index + 1], bList[index]

            if order == -1 and (bList[index] < bList[index + 1]):
                bList[index], bList[index + 1] = bList[index + 1], bList[index]

    return bList