# Remote-Container Development Environment Prototype

## Quick Start Ubuntu
```sh
sudo apt install docker.io code
code --install-extension ms-vscode-remote.remote-containers
git clone https://gitlab.com/Jason-Thomas/rc-dev-env.git
```
## Quick Start Windows
1. Download and install [VSCode](https://code.visualstudio.com/)
2. Download and install the Remote-Container extention from the VSCode pluggin store (or install the extension in powershell)
```sh
code --install-extension ms-vscode-remote.remote-containers
```
3. Download and install [Docker Desktop](https://www.docker.com/products/docker-desktop) and make sure you login with your username (not email) and password. Make sure in the Docker Desktop setting that you share the project directory that you will be mounting the container to.
4. Git clone the necessary repos
```sh
git clone https://gitlab.com/Jason-Thomas/rc-dev-env.git
```

### Step by Step
- Start VSCode and "Open Folder..." to the cloned repo
  - Once the project is open a notification toast will appear in the bottom right corner of the VSCode window. Click "Reopen in Container"
    - Alternatively you can click the green button at the bottom left of the vscode window and click the same option
  - Wait Patiently... It may take 3-8 minutes depending on your hardware and network connection
- Once VSCode is running normally, you will be able to run tasks to execute "Clean", "Build C", and "Compile C". Debugging can be run with the "Test C", "Test Python" Debug configurations in the debug tab on the left.

NOTE: Running "Test C" (along with any compilation task) and "Test Python" will require you to have the file open and focused on (in view) or the tasks will not run correctly.

### Generating a Practice test
Once the environement is all set up and ready to go it will automatically contain an initial practice test. If you wish to generate another test, run the following command in the vscode console (cntl+shift+`).
```sh
./createPracticeTest.sh
```
NOTE: All previously worked on questions will be moved to the .old_tests folder

### VSCode Tasks
Click the button labeled "Tasks" in the bottom blue bar to view tasks to execute. The task heirarchy is the following. These tasks are only for C Programming.
```sh
└─ Compile C
│   └─ Build C
└─ Clean 
```

### Features
- Lightweight and opensource C and Python development environment
- C build and compilation with CMake
- Debugging for C and Python
  - gdb debugging for C through VSCode GUI
  - Breakpoints, step through, etc.
  - Variable and Call Stack analysis
- Docker
  - Easy to deploy containerized dev environment
  - Wraps all extensions, libraries, and tools
  - It can be personalize

## Future
- TestBank CI/CD pipeline integration with Docker is possible to automate unit testing premade solutions

### Sources
- https://cmake.org/cmake/help/v3.15/
- https://code.visualstudio.com/docs/remote/containers