#pragma once
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif
	
	int bin_hex_StrToInt32(const char * s);

#ifdef __cplusplus
}
#endif